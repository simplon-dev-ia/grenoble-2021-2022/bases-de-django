# Activité sur les bases de Django

![django](https://opengraph.githubassets.com/868b3d99433f6ac6d05294a567e89eb2f17ffee2d7f4dc5e670e7bfcb8d22492/django/django)

## Recherche d'informations

- A quoi correspondent les modèles en Django ?

- A quoi correspondent les vues en Django ?

- Que contiennent les fichiers `settings.py`, `urls.py`, `manage.py`, `models.py`, `tests.py`, `views.py`, `admin.py` ?

- Qu'est-ce qu'une application Django ?

- Quels sont les points communs et les différences entre Django et Flask ?

## Ressources

La documentation officielle de Django est complète et bien conçue (y compris en français) :

- Documentation officielle en anglais (version 4.0) : https://docs.djangoproject.com/en/4.0/

- Documentation officielle en français (version 4.0) : https://docs.djangoproject.com/fr/4.0/

## Partie 1 : démarrage du projet Django

### Le projet

- Créer un environnement virtuel avec la bibliothèque Django

- Vérifier que la bibliothèque Django est bien en version 4

- Créer un projet Django à l'aide de la commande `django-admin startproject <votre nom de projet>`

- Vérifier que l'arborescence du projet Django a été créée correctement

- Vérifier le bon fonctionnement de Django en lançant le serveur de développement avec la commande `python manage.py runserver`

### L'espace d'administration

La bibliothèque Django contient nativement un espace d'administration (on peut voir le lien dans le fichier `urls.py`)

- Vérifier que l'url vers l'espace d'administration fonctionne : http://127.0.0.1:8000/admin

- Effectuer les migrations de la base de données

- Créer un compte superutilisateur (https://docs.djangoproject.com/fr/4.0/intro/tutorial02/#creating-an-admin-user)

- Vérfier l'accès à l'espace d'administration

## Partie 2 : création d'une application Django

- Créer une application Django (https://docs.djangoproject.com/fr/4.0/intro/tutorial01/#creating-the-polls-app)

- Créer une vue pour la page principale (https://docs.djangoproject.com/fr/4.0/intro/tutorial01/#write-your-first-view) à partir d'un template issu du fichier `base.html`

- Vérifier le bon fonctionnement de la vue

- Créer une seconde vue avec un accès restreint aux utilisateurs authentifiés 

## Partie 3 : création d'une application pour les comptes utilisateurs

- Créer une application pour la gestion des utilisateurs

- Créer les vues et les templates de login et logout (https://docs.djangoproject.com/en/4.0/topics/auth/default/)

- Vérifier le fonctionnement de la page en accès restreint